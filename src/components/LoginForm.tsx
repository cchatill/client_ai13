import {Button, Typography} from "@mui/material"
import Box from "@mui/material/Box"
import TextField from "@mui/material/TextField"
import React from "react"
import useAuthentication from "../modules/useAuthentication"
import {useNavigate} from "react-router-dom";

export default function LoginForm() {
    const [email, setEmail] = React.useState("")
    const [password, setPassword] = React.useState("")

    const {user, connect} = useAuthentication()
    const navigate = useNavigate();


    React.useEffect(() => {
        if (!user) return
        console.log(user.role)
        if (user.role == "TRAINEE") {
            navigate("/trainee_panel")
        } else {
            navigate("/admin_board")
        }
    }, [user])

    return (
        <Box
            component="form"
            sx={{
                "& .MuiTextField-root": {m: 2, width: "100%"},
                p: 2,
            }}
            noValidate
            autoComplete="off"
        >
            <div
                style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    padding: "10%",
                }}
            >
                <Typography variant="h4" gutterBottom component="div">
                    Connexion
                </Typography>
                <div
                    style={{
                        width: "35%",
                        minWidth: "300px",
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                        border: "1px solid grey",
                        padding: 10,
                        borderRadius: "5px",
                    }}
                >
                    <TextField
                        required
                        id="outlined-required"
                        label="Email"
                        value={email}
                        onChange={(e) => {
                            setEmail(e.target.value)
                        }}
                    />
                    <TextField
                        id="outlined-password-input"
                        label="Mot de passe"
                        type="password"
                        autoComplete="current-password"
                        value={password}
                        onChange={(e) => {
                            setPassword(e.target.value)
                        }}
                    />
                    <Button
                        variant="contained"
                        style={{
                            width: "100%",
                        }}
                        onClick={() => {
                            connect(email, password)
                        }}
                    >
                        Connexion
                    </Button>
                </div>
            </div>
        </Box>
    )
}
