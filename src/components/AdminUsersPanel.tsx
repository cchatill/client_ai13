import * as React from 'react';
import SearchIcon from '@mui/icons-material/Search';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import {styled, alpha} from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';
import Button from '@mui/material/Button';
import {Grid, IconButton, Tooltip, Typography} from '@mui/material';
import {DataGrid, GridActionsCellItem, GridRowId} from '@mui/x-data-grid';
import {Link, useNavigate} from 'react-router-dom';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import useRequest from "../modules/useRequest";

const Search = styled('div')(({theme}) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
        backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(1),
        width: '100%',
    },
}));

const SearchIconWrapper = styled('div')(({theme}) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({theme}) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '12ch',
            '&:focus': {
                width: '20ch',
            },
        },
    },
}));

export default function AdminUsersPanel() {

    const [rows, setRows] = React.useState([]);
    const {get, del} = useRequest()

    async function updateUser() {
        try {
            setRows(await get("/user/all"));
        } catch (e) {
            console.error(e)
        }
    }

    React.useEffect(() => {
        updateUser()
    }, [])

    const navigate = useNavigate();
    const goToUserPage = (id: GridRowId) => {
        console.log("Going to user " + id);
        navigate(`/admin_board_users/${id}`);
    };

    const deleteUser = React.useCallback(
        (id: GridRowId) => async () => {

            try {
                await del(`/user/byId/${id}`)
                updateUser();
            } catch (e) {
                console.error(e)
            }

            console.log('Deleting ' + id)
        },
        [],
    );

    const editUser = React.useCallback(
        (id: GridRowId) => () => {
            goToUserPage(id);
        },
        [],
    );

    const columns = React.useMemo(
        () => [
            {field: 'id', headerName: 'ID', minWidth: 70, flex: 1},
            {field: 'firstname', headerName: 'Prénom', minWidth: 170, flex: 1},
            {field: 'lastname', headerName: 'Nom', minWidth: 100, flex: 1},
            {field: 'company', headerName: 'Entreprise', minWidth: 100, flex: 1},
            {
                field: 'action',
                headerName: 'Action',
                minWidth: 70,
                flex: 1,
                type: 'actions',
                getActions: (params: { id: GridRowId; }) => [
                    <GridActionsCellItem
                        icon={<Tooltip title="Éditer">
                            <IconButton>
                                <EditIcon color="primary"/>
                            </IconButton>
                        </Tooltip>}
                        label="Edit"
                        onClick={editUser(params.id)}
                    />,
                    <GridActionsCellItem
                        icon={<Tooltip title="Supprimer">
                            <IconButton>
                                <DeleteIcon color="secondary"/>
                            </IconButton>
                        </Tooltip>}
                        label="Delete"
                        onClick={deleteUser(params.id)}
                    />,
                ],
            }], [editUser, deleteUser]);


    return (
        <div style={{
            // display: "flex",
            flexDirection: "column",
            alignItems: "center",
            paddingLeft: "10%",
            paddingRight: "10%"
        }}>
            <Tooltip title="Retour au menu principal">
                <IconButton component={Link} to="/admin_board">
                    <ArrowBackIcon color="primary"/>
                </IconButton>
            </Tooltip>
            <Typography variant="h4" gutterBottom component="div">
                Gestion des stagiaires
            </Typography>
            <Grid container spacing={2}>
                <Grid item xs={8}>
                    <Search>
                        <SearchIconWrapper>
                            <SearchIcon/>
                        </SearchIconWrapper>
                        <StyledInputBase
                            placeholder="Rechercher…"
                            inputProps={{'aria-label': 'search'}}
                        />
                    </Search>
                </Grid>
                <Grid item xs={4}>
                    <Grid container direction="row-reverse">
                        <Grid item><Button variant="contained" component={Link} to="/admin_board/create_user">Nouveau
                            stagiaire</Button></Grid>
                    </Grid>
                </Grid>
            </Grid>
            <div style={{height: 600, width: '100%'}}>
                <DataGrid
                    rows={rows}
                    columns={columns}
                    pageSize={10}
                    rowsPerPageOptions={[5]}
                    disableColumnMenu
                    onRowClick={(rowData) => goToUserPage(rowData.id)}
                />
            </div>
        </div>
    );
}