import useAuthentication from "../../modules/useAuthentication";
import {useNavigate} from "react-router-dom";
import React from "react";
import {DefaultComponentProps} from "@mui/material/OverridableComponent";

interface adminProps extends DefaultComponentProps<any> {
}

export default function Trainee(props: adminProps) {
    const {connected} = useAuthentication()
    const navigate = useNavigate();

    React.useEffect(() => {
        if (!connected) {
            navigate("/unauthorized")
        }
    }, [connected])

    return (connected && props.children)
}