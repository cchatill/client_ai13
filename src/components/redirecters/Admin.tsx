import useAuthentication from "../../modules/useAuthentication";
import {useNavigate} from "react-router-dom";
import React from "react";
import {DefaultComponentProps} from "@mui/material/OverridableComponent";

interface adminProps extends DefaultComponentProps<any> {
}

export default function Admin(props: adminProps) {
    const {connected, user} = useAuthentication()
    const navigate = useNavigate();

    React.useEffect(() => {
        if (!user || user.role == "TRAINEE") {
            navigate("/unauthorized")
        }
    })

    return (connected && props.children)
}