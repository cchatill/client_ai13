import {
    alpha,
    Box,
    Button,
    FormControlLabel,
    IconButton,
    InputBase,
    styled,
    Switch,
    Tab,
    Tabs,
    TextField,
    Tooltip,
    Typography
} from "@mui/material";
import {Link, useNavigate, useParams} from "react-router-dom";
import * as React from 'react';
import {DataGrid, GridActionsCellItem, GridRowId} from "@mui/x-data-grid";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import SearchIcon from '@mui/icons-material/Search';
import useAuthentication from "../modules/useAuthentication";
import useRequest from "../modules/useRequest";

export default function TaineeResultsPanel() {


    const {user} = useAuthentication()
    const [rows, setRows] = React.useState([]);
    const {get} = useRequest()

    async function updateQuizzes() {
        try {
            //TODO : faire dépendre l'id du user
            const data = await get(`/user/getRecords/${user?.id}`)
            const detailsRows = data.map((d: any) => {
                return {
                    id: d.id,
                    quizz: d.quizz.title,
                    theme: d.quizz.theme.label,
                    score: d.score,
                    duration: d.duration
                }
            });
            setRows(detailsRows);
        } catch (e) {
            console.error(e)
        }
    }

    React.useEffect(() => {
        if (user) {
            updateQuizzes()
        }
    }, [user])

    const navigate = useNavigate();
    const goToQuizzResultPage = (id: GridRowId) => {
        console.log("Going to quizz " + id);
        navigate(`/trainee_panel/quizz_results/${id}`);
    };

    const viewQuizz = React.useCallback(
        (id: GridRowId) => () => {
            goToQuizzResultPage(id);
        },
        [],
    );

    const columns = React.useMemo(
        () => [
            {field: 'id', headerName: 'ID', minWidth: 70, flex: 1},
            {field: 'quizz', headerName: 'Quizz', minWidth: 170, flex: 1},
            {field: 'theme', headerName: 'Thèmes', minWidth: 100, flex: 1},
            {field: 'score', headerName: 'Score', minWidth: 100, flex: 1},
            {field: 'duration', headerName: 'Durée', minWidth: 100, flex: 1},
            {
                field: 'action',
                headerName: 'Action',
                minWidth: 100,
                flex: 1,
                type: 'actions',
                getActions: (params: { id: GridRowId; }) => [
                    <GridActionsCellItem
                        icon={<Tooltip title="Voir les résultats"><SearchIcon color="primary"/></Tooltip>}
                        label="View results"
                        onClick={viewQuizz(params.id)}
                    />,
                ],
            }
        ], [viewQuizz]);

    /*const rows = [
        { id: 1, quizz: 'Quizz 1', theme: 'Theme 1', score: '80%', duration: '10:27', rank: '2/4' },
        { id: 3, quizz: 'Quizz 3', theme: 'Theme 2', score: '92%', duration: '57:40', rank: '1/9' },
    ];*/

    return (
        <div style={{
            // display: "flex",
            flexDirection: "column",
            alignItems: "center",
            paddingLeft: "10%",
            paddingRight: "10%"
        }}>
            <Tooltip title="Retour au menu principal">
                <IconButton component={Link} to="/trainee_panel">
                    <ArrowBackIcon color="primary"/>
                </IconButton>
            </Tooltip>
            <Typography variant="h4" gutterBottom component="div">
                Résultats des quizz passés
            </Typography>
            <Box sx={{width: '100%'}}>
                <div style={{height: 300, width: '100%'}}>
                    <DataGrid
                        rows={rows}
                        columns={columns}
                        pageSize={10}
                        rowsPerPageOptions={[5]}
                        disableColumnMenu
                        onRowClick={(rowData) => goToQuizzResultPage(rowData.id)}
                    />
                </div>
            </Box>
        </div>
    );
}