import {
    alpha,
    Box,
    Button,
    FormControlLabel,
    IconButton,
    InputBase,
    styled,
    Switch,
    Tab,
    Tabs,
    TextField,
    Tooltip,
    Typography
} from "@mui/material";
import {Link, useNavigate, useParams} from "react-router-dom";
import * as React from 'react';
import {DataGrid, GridActionsCellItem, GridRowId} from "@mui/x-data-grid";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import PlayCircleOutlineIccon from '@mui/icons-material/PlayCircleOutline';
import SearchIcon from '@mui/icons-material/Search'
import useRequest from "../modules/useRequest";

const Search = styled('div')(({theme}) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
        backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(1),
        width: '100%',
    },
}));

const SearchIconWrapper = styled('div')(({theme}) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({theme}) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '12ch',
            '&:focus': {
                width: '20ch',
            },
        },
    },
}));

export default function AvailableQuizzPanel() {

    const [rows, setRows] = React.useState([]);
    const {get} = useRequest()

    async function updateQuizzes() {
        try {
            const data = await get("/quizz/byIsActive/true")
            const detailsRows = data.map((d: any) => {
                return {
                    id: d.id,
                    quizz: d.title,
                    theme: d.theme.label,
                }
            });
            setRows(detailsRows);
        } catch (e) {
            console.error(e)
        }
    }

    React.useEffect(() => {
        updateQuizzes()
    }, [])


    const navigate = useNavigate();
    const goToQuizzPage = (id: GridRowId) => {
        console.log("Going to quizz " + id);
        navigate(`/trainee_panel/quizz/${id}`);
    };

    const goToQuizz = React.useCallback(
        (id: GridRowId) => () => {
            goToQuizzPage(id);
        },
        [],
    );

    const columns = React.useMemo(
        () => [
            {field: 'id', headerName: 'ID', minWidth: 70, flex: 1},
            {field: 'quizz', headerName: 'Quizz', minWidth: 170, flex: 1},
            {field: 'theme', headerName: 'Thèmes', minWidth: 100, flex: 1},
            {
                field: 'action',
                headerName: 'Action',
                minWidth: 100,
                flex: 1,
                type: 'actions',
                getActions: (params: { id: GridRowId; }) => [
                    <GridActionsCellItem
                        icon={<Tooltip title="Aller au quizz"><PlayCircleOutlineIccon color="primary"/></Tooltip>}
                        label="Go to quizz"
                        onClick={goToQuizz(params.id)}
                    />,
                ],
            }
        ], [goToQuizz]);

    /*const rows = [
        { id: 1, quizz: 'Quizz 1', theme: 'Theme 1' },
        { id: 3, quizz: 'Quizz 3', theme: 'Theme 2' },
    ];*/

    return (
        <div style={{
            // display: "flex",
            flexDirection: "column",
            alignItems: "center",
            paddingLeft: "10%",
            paddingRight: "10%"
        }}>
            <Tooltip title="Retour au menu principal">
                <IconButton component={Link} to="/trainee_panel">
                    <ArrowBackIcon color="primary"/>
                </IconButton>
            </Tooltip>
            <Typography variant="h4" gutterBottom component="div">
                Quizz disponibles
            </Typography>
            <Search>
                <SearchIconWrapper>
                    <SearchIcon/>
                </SearchIconWrapper>
                <StyledInputBase
                    placeholder="Rechercher…"
                    inputProps={{'aria-label': 'search'}}
                />
            </Search>
            <Box sx={{width: '100%'}}>
                <div style={{height: 300, width: '100%'}}>
                    <DataGrid
                        rows={rows}
                        columns={columns}
                        pageSize={10}
                        rowsPerPageOptions={[5]}
                        disableColumnMenu
                        onRowClick={(rowData) => goToQuizzPage(rowData.id)}
                    />
                </div>
            </Box>
        </div>
    );
}