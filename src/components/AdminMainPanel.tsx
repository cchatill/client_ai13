import { Button, Card, CardContent, Grid, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import React from "react";
import { Link } from "react-router-dom";
import PeopleAltIcon from '@mui/icons-material/PeopleAlt';
import PeopleAltOutlinedIcon from '@mui/icons-material/PeopleAltOutlined';
import HelpIcon from '@mui/icons-material/Help';
import RuleIcon from '@mui/icons-material/Rule';

export default function AdminMainPanel() {

    return (<Box
        component="form"
        sx={{
            '& .MuiTextField-root': { m: 2, width: '100%' },
            p: 2,
        }}
        noValidate
        autoComplete="off"
    >
        <Card sx={{ minWidth: 275, marginBottom: 2 }}>
            <CardContent>
                <Typography sx={{ fontSize: 16 }} color="text.secondary" gutterBottom>
                    Utilisateur connecté
                </Typography>
                <Typography variant="h4" component="div">
                    "Utilisateur"
                </Typography>
                <Typography sx={{ mb: 1.5 }} color="text.secondary">
                    Administrateur
                </Typography>
                <Typography variant="body1">
                    Bienvenue sur l'espace d'évaluation des stagiaires.
                </Typography>
            </CardContent>
        </Card>
        <Grid container spacing={4} sx={{ marginBottom: 2 }}>
            <Grid item xs={3}>
                <Card sx={{ minWidth: 70 }}>
                    <CardContent>
                        <PeopleAltIcon color="primary" />
                        <Typography sx={{ fontSize: 16 }} color="text.secondary" gutterBottom>
                            Utilisateurs actifs
                        </Typography>
                        <Typography variant="h4" component="div">
                            9
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
            <Grid item xs={3}>
                <Card sx={{ minWidth: 70 }}>
                    <CardContent>
                        <PeopleAltOutlinedIcon color="primary" />
                        <Typography sx={{ fontSize: 16 }} color="text.secondary" gutterBottom>
                            Utilisateurs inactifs
                        </Typography>
                        <Typography variant="h4" component="div">
                            2
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
            <Grid item xs={3}>
                <Card sx={{ minWidth: 70 }}>
                    <CardContent>
                        <HelpIcon color="primary" />
                        <Typography sx={{ fontSize: 16 }} color="text.secondary" gutterBottom>
                            Nombre de quizz
                        </Typography>
                        <Typography variant="h4" component="div">
                            4
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
            <Grid item xs={3}>
                <Card sx={{ minWidth: 70 }}>
                    <CardContent>
                        <RuleIcon color="primary" />
                        <Typography sx={{ fontSize: 16 }} color="text.secondary" gutterBottom>
                            Nombre d'évaluations
                        </Typography>
                        <Typography variant="h4" component="div">
                            2
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
        </Grid>
        <Grid container spacing={2}>
            <Grid item xs={6}>
                <Card sx={{ minWidth: 170 }}>
                    <CardContent>
                        <Button size="large" variant="contained" component={Link} to="/admin_board_users" style={{
                            width: "100%"
                        }}>
                            Gérer les stagiaires
                        </Button>
                    </CardContent>
                </Card>
            </Grid>
            <Grid item xs={6}>
                <Card sx={{ minWidth: 170 }}>
                    <CardContent>
                        <Button size="large" variant="contained" component={Link} to="/admin_board_quizz" style={{
                            width: "100%"
                        }}>
                            Gérer les quizz
                        </Button>
                    </CardContent>
                </Card>
            </Grid>
        </Grid>
    </Box>);
}