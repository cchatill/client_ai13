import * as React from "react";
import {Box, Button, IconButton, TextField, Tooltip, Typography} from "@mui/material";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import {Link, useNavigate} from "react-router-dom";
import useRequest from "../modules/useRequest";

export default function CreateUserPanel() {
    const navigate = useNavigate();

    const {post} = useRequest()

    const [firstname, setFirstName] = React.useState("");
    const [lastname, setLastName] = React.useState("");
    const [email, setEmail] = React.useState("");
    const [phone, setPhone] = React.useState("");
    const [company, setCompany] = React.useState("");
    const [password, setPassword] = React.useState("");
    return (
        <Box
            component="form"
            sx={{
                '& .MuiTextField-root': {m: 2, width: '100%'},
                p: 2,
            }}
            noValidate
            autoComplete="off"
        >
            <Tooltip title="Retour à la gestion des stagiaires">
                <IconButton component={Link} to="/admin_board_users">
                    <ArrowBackIcon color="primary"/>
                </IconButton>
            </Tooltip>
            <Typography variant="h4" gutterBottom component="div">
                Création d'un stagiaire
            </Typography>
            <div style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center"
            }}>
                <div style={{
                    width: "55%",
                    minWidth: "300px",
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    border: '1px solid grey',
                    padding: 10,
                    borderRadius: "5px"
                }}>
                    <TextField
                        required
                        id="outlined-required"
                        label="Prénom"
                        value={firstname}
                        onChange={(e) => {
                            setFirstName(e.target.value);
                        }}
                    />
                    <TextField
                        required
                        id="outlined-required"
                        label="Nom"
                        value={lastname}
                        onChange={(e) => {
                            setLastName(e.target.value);
                        }}
                    />
                    <TextField
                        required
                        id="outlined-required"
                        label="Email"
                        value={email}
                        onChange={(e) => {
                            setEmail(e.target.value);
                        }}
                    />
                    <TextField
                        required
                        id="outlined-required"
                        label="Téléphone"
                        value={phone}
                        onChange={(e) => {
                            setPhone(e.target.value);
                        }}
                    />
                    <TextField
                        required
                        id="outlined-required"
                        label="Entreprise"
                        value={company}
                        onChange={(e) => {
                            setCompany(e.target.value);
                        }}
                    />
                    <TextField
                        id="outlined-password-input"
                        label="Mot de passe"
                        type="password"
                        autoComplete="current-password"
                        value={password}
                        onChange={(e) => {
                            setPassword(e.target.value);
                        }}
                    />
                    <Button variant="contained" style={{
                        width: "100%",
                    }}
                            onClick={async () => {
                                try {
                                    await post("/user/addTrainee", {
                                        lastname,
                                        firstname,
                                        email,
                                        password,
                                        company,
                                        phone,
                                        active: true
                                    })
                                    navigate("/admin_board_users");
                                } catch (e) {
                                    console.error(e)
                                }

                            }}>
                        Créer le stagiaire
                    </Button>
                </div>
            </div>
        </Box>
    );
}