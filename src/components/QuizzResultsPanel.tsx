import {
    Tooltip,
    Typography,
    IconButton,
    Card,
    CardContent,
    Grid,
    Box,
    Button,
    Step,
    StepLabel,
    Stepper,
    ToggleButton,
    ToggleButtonGroup,
    List,
    ListItem,
    ListItemText,
    ListItemIcon
} from "@mui/material";
import {Link, useParams} from "react-router-dom";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import RuleIcon from '@mui/icons-material/Rule';
import TimerIcon from '@mui/icons-material/Timer';
import React from "react";
import CloseIcon from '@mui/icons-material/Close';
import DoneIcon from '@mui/icons-material/Done';
import {ResponseType} from "../modules/types";
import useRequest from "../modules/useRequest";

//const steps = ['Question 1', 'Question 2', 'Question 3', 'Question 4', 'Question 5'];

export function QuizzResultsPanel() {
    const {id} = useParams();
    const {get} = useRequest()

    const [quizzId, setQuizzId] = React.useState(999);
    const [title, setTitle] = React.useState("");
    const [theme, setTheme] = React.useState("");
    const [score, setScore] = React.useState("");
    const [nbOfQuestions, setNbOfQuestions] = React.useState(0);
    const [duration, setDuration] = React.useState("");
    const [steps, setSteps] = React.useState([""]);

    const [userAnswers, setUserAnswers] = React.useState([]);
    const [answers, setAnswers] = React.useState<ResponseType[][]>([[]]);

    async function initPage() {
        try {
            //Infos de base
            const data = await get(`/record/byId/${id}`)
            const QuizzId = data.quizz.id;
            setQuizzId(QuizzId);
            setTitle(data.quizz.title);
            setTheme(data.quizz.theme.label);
            const nbQuestion = data.answers.length;
            setNbOfQuestions(nbQuestion);
            setScore((data.score / nbQuestion * 100).toFixed(0) + "%");
            const minutes = Math.floor(data.duration / 60);
            const secondes = data.duration % 60;
            setDuration(minutes.toString() + ":" + secondes);
            setUserAnswers(data.answers);

            //Question
            const data2 = await get(`/question/byQuizzId/${QuizzId}`);
            let questionsTitle: string[] = [];
            for (let i = 0; i < nbQuestion; i++) {
                questionsTitle.push(data2[i].label);
            }
            setSteps(questionsTitle);


            //Réponses
            const data3 = await get<ResponseType[][]>(`/answer/byQuizzId/${QuizzId}`)
            let answersList = [];
            for (let i = 0; i < nbQuestion; i++) {
                const nbAnswer = data3[i].length;
                let answersForThisQuestion = []
                for (let j = 0; j < nbAnswer; j++) {
                    const label = data3[i][j].label;
                    const isCorrect = data3[i][j].isCorrect
                    const pos = data3[i][j].position;
                    let isSelected = "";
                    if (data.answers[i].position == pos)
                        isSelected = "Réponse sélectionnée";
                    answersForThisQuestion.push({
                        "label": label,
                        "isCorrect": isCorrect,
                        "isSelected": isSelected,
                        position: pos
                    });
                }
                answersForThisQuestion.sort((a, b) => {
                    return a.position > b.position ? 1 : -1
                })
                answersList.push(answersForThisQuestion);
            }


            setAnswers(answersList);
            console.log(answersList);


        } catch (e) {
            console.error(e)
        }
    }

    React.useEffect(() => {
        initPage()
    }, [])


    const [activeStep, setActiveStep] = React.useState(0);
    const [skipped, setSkipped] = React.useState(new Set<number>());

    const isStepSkipped = (step: number) => {
        return skipped.has(step);
    };

    const handleNext = () => {
        let newSkipped = skipped;
        if (isStepSkipped(activeStep)) {
            newSkipped = new Set(newSkipped.values());
            newSkipped.delete(activeStep);
        }

        setActiveStep((prevActiveStep) => prevActiveStep + 1);
        setSkipped(newSkipped);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleSkip = () => {
        // if (!isStepOptional(activeStep)) {
        //   // You probably want to guard against something like this,
        //   // it should never occur unless someone's actively trying to break something.
        //   throw new Error("Vous ne pouvez pas passer cette question.");
        // }

        setActiveStep((prevActiveStep) => prevActiveStep + 1);
        setSkipped((prevSkipped) => {
            const newSkipped = new Set(prevSkipped.values());
            newSkipped.add(activeStep);
            return newSkipped;
        });
    };

    const handleReset = () => {
        setActiveStep(0);
    };

    function generate(element: React.ReactElement) {
        return [0, 1, 2].map((value) =>
            React.cloneElement(element, {
                key: value,
            }),
        );
    }

    return (
        <div style={{
            // display: "flex",
            flexDirection: "column",
            alignItems: "center",
            paddingLeft: "10%",
            paddingRight: "10%"
        }}>
            <Tooltip title="Retour aux résultats des quizz">
                <IconButton component={Link} to="/trainee_panel/results">
                    <ArrowBackIcon color="primary"/>
                </IconButton>
            </Tooltip>
            <Typography variant="h4" gutterBottom component="div">
                Résultats du quizz
            </Typography>
            <Grid container spacing={3} sx={{marginBottom: 2}}>
                <Grid item xs={4}>
                    <Card sx={{minWidth: 70, marginBottom: 2}}>
                        <CardContent>
                            <Typography sx={{fontSize: 16}} color="text.secondary" gutterBottom>
                                "Stagiaire"
                            </Typography>
                            <Typography variant="h4" component="div">
                                {title}
                            </Typography>
                            <Typography sx={{mb: 1.5}} color="text.secondary">
                                {theme}
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={4}>
                    <Card sx={{minWidth: 70}}>
                        <CardContent>
                            <RuleIcon color="primary"/>
                            <Typography sx={{fontSize: 16}} color="text.secondary" gutterBottom>
                                Score
                            </Typography>
                            <Typography variant="h4" component="div">
                                {score}
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={4}>
                    <Card sx={{minWidth: 70}}>
                        <CardContent>
                            <TimerIcon color="primary"/>
                            <Typography sx={{fontSize: 16}} color="text.secondary" gutterBottom>
                                Temps
                            </Typography>
                            <Typography variant="h4" component="div">
                                {duration}
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
            <Stepper activeStep={activeStep}>
                {steps.map((label, index) => {
                    const stepProps: { completed?: boolean } = {};
                    const labelProps: {
                        optional?: React.ReactNode;
                    } = {};
                    // if (isStepOptional(index)) {
                    //   labelProps.optional = (
                    //     <Typography variant="caption">Optional</Typography>
                    //   );
                    // }
                    if (isStepSkipped(index)) {
                        stepProps.completed = false;
                    }
                    return (
                        <Step key={label} {...stepProps}>
                            <StepLabel {...labelProps}>{label}</StepLabel>
                        </Step>
                    );
                })}
            </Stepper>
            {activeStep === steps.length ? (
                <React.Fragment>
                    <Typography variant="h6" sx={{mt: 2, mb: 1}}>
                        Vous avez parcouru la correction du quizz.
                    </Typography>
                    <Box sx={{display: 'flex', flexDirection: 'row', pt: 2}}>
                        <Box sx={{flex: '1 1 auto'}}/>
                        <Button onClick={handleReset}>Revoir</Button>
                    </Box>
                </React.Fragment>
            ) : (
                <React.Fragment>
                    <Typography variant="h6" sx={{mt: 2, mb: 1}}>{steps[activeStep]}</Typography>
                    <List>
                        {answers[activeStep].map((reponse) => (
                            <ListItem key={reponse.position}>
                                <ListItemIcon>
                                    {reponse.isCorrect ?
                                        <DoneIcon color="success"/>
                                        : <CloseIcon color="error"/>
                                    }
                                </ListItemIcon>
                                <ListItemText
                                    primary={reponse.label}
                                    secondary={reponse.isSelected}
                                />
                            </ListItem>
                        ))}
                    </List>
                    <Box sx={{display: 'flex', flexDirection: 'row', pt: 2}}>
                        <Button
                            color="inherit"
                            disabled={activeStep === 0}
                            onClick={handleBack}
                            sx={{mr: 1}}
                        >
                            Précédent
                        </Button>
                        <Box sx={{flex: '1 1 auto'}}/>
                        {/* {isStepOptional(activeStep) && (
                  <Button color="inherit" onClick={handleSkip} sx={{ mr: 1 }}>
                    Passer
                  </Button>
                )} */}
                        <Button onClick={handleNext}>
                            {activeStep === steps.length - 1 ? 'Terminer' : 'Suivant'}
                        </Button>
                    </Box>
                </React.Fragment>
            )}
        </div>
    );
}