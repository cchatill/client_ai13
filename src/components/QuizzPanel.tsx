import * as React from 'react';
import Box from '@mui/material/Box';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import {Link, useParams} from 'react-router-dom';
import {
    ToggleButton,
    ToggleButtonGroup,
    Card,
    CardContent,
    Collapse,
} from '@mui/material';
import TimerIcon from '@mui/icons-material/Timer';
import Timer, {getTimeParts} from 'react-compound-timerv2';
import {ResponseType} from '../modules/types';
import useAuthentication from '../modules/useAuthentication';
import useRequest from "../modules/useRequest";


export default function QuizzPanel() {
    const {id} = useParams();
    const {user} = useAuthentication()
    const {get, post} = useRequest()

    const [activeStep, setActiveStep] = React.useState(0);
    const [skipped, setSkipped] = React.useState(new Set<number>());

    const [startQ, setStartQ] = React.useState(false);

    const [title, setTitle] = React.useState("");
    const [theme, setTheme] = React.useState("");
    const [steps, setSteps] = React.useState([""]);
    const [duration, setDuration] = React.useState(0);
    const [nbOfQuestions, setNbOfQuestion] = React.useState(0)

    //TODO : lien entre ces variables et les questions/réponses à afficher
    const [answers, setAnswers] = React.useState<ResponseType[][]>([[]]);

    async function initPage() {
        try {
            //Infos de base
            const quiz = await get(`/quizz/byId/${id}`);
            setTitle(quiz.title);
            setTheme(quiz.theme.label);

            //Question
            const questions = await get(`/question/byQuizzId/${id}`);
            const nbQuestions = questions.length
            setNbOfQuestion(nbQuestions)
            let questionsTitle: string[] = [];
            for (let i = 0; i < nbQuestions; i++) {
                questionsTitle.push(questions[i].label);
            }
            setSteps(questionsTitle);


            //Réponses
            const answers = await get<ResponseType[][]>(`/answer/byQuizzId/${id}`)
            let answersList = [];
            for (let i = 0; i < nbQuestions; i++) {
                const nbAnswer = answers[i].length;
                let answersForThisQuestion = []
                for (let j = 0; j < nbAnswer; j++) {
                    const answerId = answers[i][j].id;
                    const label = answers[i][j].label;
                    const pos = answers[i][j].position;
                    answersForThisQuestion.push({"label": label, position: pos, id: answerId});
                }
                answersForThisQuestion.sort((a, b) => {
                    return a.position > b.position ? 1 : -1
                })
                answersList.push(answersForThisQuestion);
            }


            setAnswers(answersList);


        } catch (e) {
            console.error(e)
        }
    }

    React.useEffect(() => {
        initPage()
    }, [])

    const startQuizz = () => {
        setStartQ((prev) => !prev);
    };

    const [disabledStartButton, setDisabledStartButton] = React.useState(false);

    const handleStartButton = (event: any) => {
        if (disabledStartButton)
            return;
        setDisabledStartButton(true);
    }

    const [answersList, setAnswersList] = React.useState<number[]>([]);
    const [answer, setAnswer] = React.useState(0);

    const handleAnswerChange = (
        event: React.MouseEvent<HTMLElement>,
        newAnswer: number,
    ) => {
        //setAnswersList([...answersList,newAnswer])
        setAnswer(newAnswer)
    };

    // const isStepOptional = (step: number) => {
    //   return step === 1;
    // };

    const isStepSkipped = (step: number) => {
        return skipped.has(step);
    };

    const handleNext = () => {
        let newSkipped = skipped;
        if (isStepSkipped(activeStep)) {
            newSkipped = new Set(newSkipped.values());
            newSkipped.delete(activeStep);
        }
        setAnswersList([...answersList, answer])
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
        setSkipped(newSkipped);
        setAnswer(0)
    };

    //TODO : certaines fois, la durée reste à 0
    async function submitQuizz() {
        await post("/quizz/submit", {
            score: 0, //le score est calculé par le backend
            duration,
            userId: user?.id,
            quizzId: id,
            answersId: answersList
        })
    }

    React.useEffect(() => {
        if (activeStep == nbOfQuestions && activeStep != 0) {
            console.log("ENVOi des réponses");
            submitQuizz();
        }
    }, [answersList])

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleSkip = () => {
        // if (!isStepOptional(activeStep)) {
        //   // You probably want to guard against something like this,
        //   // it should never occur unless someone's actively trying to break something.
        //   throw new Error("Vous ne pouvez pas passer cette question.");
        // }

        setActiveStep((prevActiveStep) => prevActiveStep + 1);
        setSkipped((prevSkipped) => {
            const newSkipped = new Set(prevSkipped.values());
            newSkipped.add(activeStep);
            return newSkipped;
        });
    };

    const handleReset = () => {
        setActiveStep(0);
    };

    return (
        <div style={{
            // display: "flex",
            flexDirection: "column",
            alignItems: "center",
            paddingLeft: "10%",
            paddingRight: "10%"
        }}>
            <Timer startImmediately={false} onStop={() => console.log("onStop hook")}>{({
                                                                                            start,
                                                                                            stop,
                                                                                            getTime
                                                                                        }: { start: any, stop: any, getTime: any }) => (
                <React.Fragment>
                    <Typography variant="h4" gutterBottom component="div">
                        {title}
                        <ToggleButtonGroup
                            color="primary"
                            orientation="vertical"
                            value={start}
                            exclusive
                            onChange={startQuizz}
                            sx={{marginLeft: 2}}
                        >
                            <ToggleButton size="large" value="start" onClick={(event) => {
                                handleStartButton(event);
                                start();
                            }} disabled={disabledStartButton}>Commencer</ToggleButton>
                        </ToggleButtonGroup>
                    </Typography>
                    <Collapse in={startQ}>
                        <Box sx={{width: '100%'}}>
                            <Card sx={{minWidth: 275, marginBottom: 2, textAlign: "center"}}>
                                <CardContent>
                                    <TimerIcon/>
                                    <Typography sx={{fontSize: 16}} color="text.secondary">
                                        Temps écoulé : <span style={{fontWeight: 'bold'}}>
                      <Timer.Hours/>h<Timer.Minutes/>m<Timer.Seconds/>s
                    </span>
                                    </Typography>
                                </CardContent>
                            </Card>
                            <Stepper activeStep={activeStep}>
                                {steps.map((label, index) => {
                                    const stepProps: { completed?: boolean } = {};
                                    const labelProps: {
                                        optional?: React.ReactNode;
                                    } = {};
                                    // if (isStepOptional(index)) {
                                    //   labelProps.optional = (
                                    //     <Typography variant="caption">Optional</Typography>
                                    //   );
                                    // }
                                    if (isStepSkipped(index)) {
                                        stepProps.completed = false;
                                    }
                                    return (
                                        <Step key={label} {...stepProps}>
                                            <StepLabel {...labelProps}>{label}</StepLabel>
                                        </Step>
                                    );
                                })}
                            </Stepper>
                            {activeStep === steps.length ? (
                                <React.Fragment>
                                    <Typography variant="h6" sx={{mt: 2, mb: 1}}>
                                        Vous avez terminé le quizz.
                                    </Typography>
                                    <Box sx={{display: 'flex', flexDirection: 'row', pt: 2}}>
                                        <Box sx={{flex: '1 1 auto'}}/>
                                        <Button /*onClick={handleReset}*/ component={Link}
                                                                          to="/trainee_panel/available_quizz">Retour aux
                                            quizz disponibles</Button>
                                    </Box>
                                </React.Fragment>
                            ) : (
                                <React.Fragment>
                                    <Typography variant="h6" sx={{mt: 2, mb: 1}}>{steps[activeStep]}</Typography>
                                    <ToggleButtonGroup
                                        color="primary"
                                        orientation="vertical"
                                        value={answer}
                                        exclusive
                                        onChange={handleAnswerChange}
                                    >
                                        {answers[activeStep].map((reponse) => (
                                            <ToggleButton value={reponse.id}
                                                          key={`${activeStep}.${reponse.position}`}>{reponse.label}</ToggleButton>
                                        ))}
                                    </ToggleButtonGroup>
                                    <Box sx={{display: 'flex', flexDirection: 'row', pt: 2}}>
                                        <Box sx={{flex: '1 1 auto'}}/>
                                        {/* {isStepOptional(activeStep) && (
                  <Button color="inherit" onClick={handleSkip} sx={{ mr: 1 }}>
                    Passer
                  </Button>
                )} */}
                                        <Button onClick={handleNext}>
                                            {activeStep === steps.length - 1 ? <span onClick={() => {
                                                setDuration(getTime() / 1000);
                                                stop();
                                                console.log(getTime())
                                            }}>Terminer</span> : 'Suivant'}
                                        </Button>
                                    </Box>
                                </React.Fragment>
                            )}
                        </Box>
                    </Collapse>
                </React.Fragment>
            )}
            </Timer>
        </div>
    );
}