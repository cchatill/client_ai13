import {
    alpha,
    Box,
    Button,
    FormControlLabel,
    IconButton,
    InputBase,
    styled,
    Switch,
    Tab,
    Tabs,
    TextField,
    Tooltip,
    Typography
} from "@mui/material";
import {Link, useNavigate, useParams} from "react-router-dom";
import * as React from 'react';
import {DataGrid, GridActionsCellItem, GridRowId} from "@mui/x-data-grid";
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import SearchIcon from '@mui/icons-material/Search';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import {Password} from "@mui/icons-material";
import useRequest from "../modules/useRequest";

const Search = styled('div')(({theme}) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
        backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(1),
        width: '100%',
    },
}));

const SearchIconWrapper = styled('div')(({theme}) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({theme}) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '12ch',
            '&:focus': {
                width: '20ch',
            },
        },
    },
}));

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function TabPanel(props: TabPanelProps) {
    const {children, value, index, ...other} = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{p: 3}}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export default function AdminEditUserPanel() {
    const {id} = useParams();
    const {get, post} = useRequest()

    const [value, setValue] = React.useState(0);
    const [firstname, setFirstName] = React.useState("");
    const [lastname, setLastName] = React.useState("");
    const [email, setEmail] = React.useState("");
    const [phone, setPhone] = React.useState("");
    const [company, setCompany] = React.useState("");
    const [status, setStatus] = React.useState(true);
    const [date, setDate] = React.useState("");
    const [rows, setRows] = React.useState([{id: 1, quizz: "", theme: "", meanScore: 0, bestScore: 0, rank: ""}]);

    React.useEffect(() => {
        (async () => {
            try {
                //Récupérer infos du user
                const user = await get(`/user/byId/${id}`)
                setFirstName(user.firstname);
                setLastName(user.lastname);
                setPhone(user.phone);
                setCompany(user.company);
                setEmail(user.email);
                setStatus(user.active);
                setDate(user.creationDate);

                //récupérer infos sur les parcours du user
                const data = await get(`/user/getRankings/${id}`);
                const detailsRows = []
                for (const d of data) {
                    const [rank1, rank2] = await get(`/quizz/ranking/${d.quizz.id}/${id}`)
                    detailsRows.push({
                        id: d.id,
                        quizz: d.quizz.title,
                        theme: d.quizz.theme.label,
                        meanScore: d.meanScore,
                        bestScore: d.bestScore,
                        rank: rank1 + "/" + rank2
                    })
                }

                setRows(detailsRows);

            } catch (e) {
                console.error(e)
            }
        })()
    }, [])

    function handleSwitchChange(e: { target: { checked: React.SetStateAction<boolean>; }; }) {
        setStatus(e.target.checked);
        // Add actions here for when the switch is triggered
    };


    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
    };

    const navigate = useNavigate();
    const goToQuizzPage = (id: GridRowId) => {
        console.log("Going to quizz " + id);
        navigate(`/admin_board_quizz/${id}`);
    };

    const deleteQuizz = React.useCallback(
        (id: GridRowId) => () => {
            console.log('Deleting ' + id)
        },
        [],
    );

    const editQuizz = React.useCallback(
        (id: GridRowId) => () => {
            goToQuizzPage(id);
        },
        [],
    );

    const columns = React.useMemo(
        () => [
            {field: 'id', headerName: 'ID', minWidth: 70, flex: 1},
            {field: 'quizz', headerName: 'Quizz', minWidth: 170, flex: 1},
            {field: 'theme', headerName: 'Thèmes', minWidth: 100, flex: 1},
            {field: 'meanScore', headerName: 'Score moyen', minWidth: 100, flex: 1},
            {field: 'bestScore', headerName: 'Meilleur score', minWidth: 100, flex: 1},
            {field: 'rank', headerName: 'Classement', minWidth: 100, flex: 1},
            // {
            //     field: 'action', headerName: 'Action', minWidth: 100, flex: 1, type: 'actions', getActions: (params: { id: GridRowId; }) => [
            //         <GridActionsCellItem
            //             icon={<EditIcon color="primary" />}
            //             label="Edit"
            //             onClick={editQuizz(params.id)}
            //         />,
            //         <GridActionsCellItem
            //             icon={<DeleteIcon color="secondary" />}
            //             label="Delete"
            //             onClick={deleteQuizz(params.id)}
            //         />,
            //     ],
            // }
        ], [editQuizz, deleteQuizz]);

    /*const rows = [
        { id: 1, quizz: 'Quizz 1', theme: 'Theme 1', score: '80%', best_score: '86%', rank: '2/4' },
        { id: 3, quizz: 'Quizz 3', theme: 'Theme 2', score: '92%', best_score: '92%', rank: '1/9' },
    ];*/

    return (
        <div style={{
            // display: "flex",
            flexDirection: "column",
            alignItems: "center",
            paddingLeft: "10%",
            paddingRight: "10%"
        }}>
            <Tooltip title="Retour à la gestion des stagiaires">
                <IconButton component={Link} to="/admin_board_users">
                    <ArrowBackIcon color="primary"/>
                </IconButton>
            </Tooltip>
            <Typography variant="h4" gutterBottom component="div">
                Stagiaire (id: {id})
            </Typography>
            <Box sx={{width: '100%'}}>
                <Box sx={{borderBottom: 1, borderColor: 'divider'}}>
                    <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                        <Tab label="Informations de l'utilisateur" {...a11yProps(0)} />
                        <Tab label="Enregistrements" {...a11yProps(1)} />
                    </Tabs>
                </Box>
                <TabPanel value={value} index={0}>
                    <Box
                        component="form"
                        sx={{
                            '& .MuiTextField-root': {m: 2, width: '100%'},
                            p: 2,
                        }}
                        noValidate
                        autoComplete="off"
                    >
                        <div style={{
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "center"
                        }}>
                            <div style={{
                                width: "35%",
                                minWidth: "300px",
                                display: "flex",
                                flexDirection: "column",
                                alignItems: "center",
                                border: '1px solid grey',
                                padding: 10,
                                borderRadius: "5px"
                            }}>
                                <TextField
                                    required
                                    id="outlined-required"
                                    label="Prénom"
                                    value={firstname}
                                    onChange={(e) => {
                                        setFirstName(e.target.value);
                                    }}
                                />
                                <TextField
                                    required
                                    id="outlined-required"
                                    label="Nom"
                                    value={lastname}
                                    onChange={(e) => {
                                        setLastName(e.target.value);
                                    }}
                                />
                                <TextField
                                    required
                                    id="outlined-required"
                                    label="Email"
                                    value={email}
                                    onChange={(e) => {
                                        setEmail(e.target.value);
                                    }}
                                />
                                <TextField
                                    required
                                    id="outlined-required"
                                    label="Téléphone"
                                    value={phone}
                                    onChange={(e) => {
                                        setPhone(e.target.value);
                                    }}
                                />
                                <TextField
                                    required
                                    id="outlined-required"
                                    label="Entreprise"
                                    value={company}
                                    onChange={(e) => {
                                        setCompany(e.target.value);
                                    }}
                                />
                                <Typography>Date de création: {date}</Typography>
                                <FormControlLabel
                                    control={<Switch checked={status} onChange={handleSwitchChange}/>}
                                    label="Actif"
                                    value={status}
                                />
                                <Button variant="contained" style={{
                                    width: "100%",
                                }}
                                        onClick={async () => {
                                            await post(`/user/update/${id}`, {
                                                firstname,
                                                lastname,
                                                email,
                                                company,
                                                phone,
                                                active: true
                                            })

                                            navigate("/admin_board_users")

                                            console.log(firstname, lastname)
                                        }}>
                                    Enregistrer les modifications
                                </Button>
                            </div>
                        </div>
                    </Box>
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <Search>
                        <SearchIconWrapper>
                            <SearchIcon/>
                        </SearchIconWrapper>
                        <StyledInputBase
                            placeholder="Rechercher…"
                            inputProps={{'aria-label': 'search'}}
                        />
                    </Search>
                    <div style={{height: 300, width: '100%'}}>
                        <DataGrid
                            rows={rows}
                            columns={columns}
                            pageSize={10}
                            rowsPerPageOptions={[5]}
                            disableColumnMenu
                            onRowClick={(rowData) => goToQuizzPage(rowData.id)}
                        />
                    </div>
                </TabPanel>
            </Box>
        </div>
    );
}