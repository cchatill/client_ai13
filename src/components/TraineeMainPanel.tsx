import { Button, Card, CardContent, Grid, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import React from "react";
import { Link } from "react-router-dom";
import PeopleAltIcon from '@mui/icons-material/PeopleAlt';
import PeopleAltOutlinedIcon from '@mui/icons-material/PeopleAltOutlined';
import HelpIcon from '@mui/icons-material/Help';
import RuleIcon from '@mui/icons-material/Rule';

export default function TraineeMainPanel() {

    return (<Box
        component="form"
        sx={{
            '& .MuiTextField-root': { m: 2, width: '100%' },
            p: 2,
        }}
        noValidate
        autoComplete="off"
    >
        <Card sx={{ minWidth: 275, marginBottom: 2 }}>
            <CardContent>
                <Typography sx={{ fontSize: 16 }} color="text.secondary" gutterBottom>
                    Utilisateur connecté
                </Typography>
                <Typography variant="h4" component="div">
                    "Utilisateur"
                </Typography>
                <Typography sx={{ mb: 1.5 }} color="text.secondary">
                    Stagiaire
                </Typography>
                <Typography variant="body1">
                    Bienvenue sur l'espace d'évaluation des stagiaires.
                </Typography>
            </CardContent>
        </Card>
        <Grid container spacing={2} sx={{ marginBottom: 2 }}>
            <Grid item xs={6}>
                <Card sx={{ minWidth: 70 }}>
                    <CardContent>
                        <HelpIcon color="primary" />
                        <Typography sx={{ fontSize: 16 }} color="text.secondary" gutterBottom>
                            Nombre de quizz passés
                        </Typography>
                        <Typography variant="h4" component="div">
                            4
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
            <Grid item xs={6}>
                <Card sx={{ minWidth: 70 }}>
                    <CardContent>
                        <RuleIcon color="primary" />
                        <Typography sx={{ fontSize: 16 }} color="text.secondary" gutterBottom>
                            Score moyen
                        </Typography>
                        <Typography variant="h4" component="div">
                            81%
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
        </Grid>
        <Grid container spacing={2}>
            <Grid item xs={6}>
                <Card sx={{ minWidth: 170 }}>
                    <CardContent>
                        <Button size="large" variant="contained" component={Link} to="/trainee_panel/available_quizz" style={{
                            width: "100%"
                        }}>
                            Voir les quizz
                        </Button>
                    </CardContent>
                </Card>
            </Grid>
            <Grid item xs={6}>
                <Card sx={{ minWidth: 170 }}>
                    <CardContent>
                        <Button size="large" variant="contained" component={Link} to="/trainee_panel/results" style={{
                            width: "100%"
                        }}>
                            Résultats
                        </Button>
                    </CardContent>
                </Card>
            </Grid>
        </Grid>
    </Box>);
}