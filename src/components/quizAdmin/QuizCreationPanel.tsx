import * as React from "react";
import {
    Box,
    IconButton,
    Tooltip,
    Typography
} from "@mui/material";
import {Link, useParams} from "react-router-dom";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import QuizBox from "./QuizBox";
import {getDefaultQuiz} from "../../modules/utils";

export default function QuizCreationPanel() {
    const {id} = useParams();

    return (
        <Box
            component="form"
            noValidate
            autoComplete="off"
            sx={{
                pl: "10%",
                pr: "10%"
            }}
        >
            <Tooltip title="Retour à la gestion des quizz">
                <IconButton component={Link} to="/admin_board_quizz">
                    <ArrowBackIcon color="primary"/>
                </IconButton>
            </Tooltip>
            <Typography variant="h4" gutterBottom component="div">
                Création d'un quizz
            </Typography>
            <div style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center"
            }}>
                <QuizBox mode="add" defaultQuiz={getDefaultQuiz()} />
            </div>
        </Box>
    );
}