import {
    Box,
    IconButton, Radio,
    TextField,
    Tooltip,
    Typography
} from "@mui/material";
import {Add, ArrowDownward, ArrowUpward} from "@mui/icons-material";
import DeleteIcon from "@mui/icons-material/Delete";
import * as React from "react";
import {AnswerType, QuestionType} from "../../modules/types";
import {getDefaultAnswer} from "../../modules/utils";

export interface QuestionEditorProps {
    question: QuestionType
    onQuestionChange: (question: QuestionType) => void
    onRemove: (id?: number) => void
}

export default function QuestionEditor(props: QuestionEditorProps) {
    const [label, setLabel] = React.useState<string>(props.question.label)

    const [answers, setAnswers] = React.useState<AnswerType[]>(props.question.answers)

    React.useEffect(() => {
        setLabel(props.question.label)
        setAnswers(props.question.answers)
    }, [props.question])

    React.useEffect(() => {
        props.onQuestionChange({...props.question, label, answers})
    }, [answers, label])

    function moveAnswerDown(index: number) {
        if (index < 0 || index >= answers.length - 1) return
        setAnswers(old => {
            const temp = [...old]
            temp.splice(index + 1, 0, temp.splice(index, 1)[0])
            return temp
        })
    }

    function moveAnswerUp(index: number) {
        if (index < 1 || index >= answers.length) return
        setAnswers(old => {
            const temp = [...old]
            temp.splice(index - 1, 0, temp.splice(index, 1)[0])
            return temp
        })
    }

    function remove(index: number) {
        if (index < 0 || index >= answers.length) return
        props.onRemove(answers[index].id)
        setAnswers(old => {
            const temp = [...old]
            temp.splice(index, 1)
            return temp
        })
    }

    function add() {
        setAnswers(old => {
            return [...old, getDefaultAnswer()]
        })
    }

    function handleAnswerChange(answer: AnswerType, index: number) {
        setAnswers(old => {
            const temp = [...old]
            if (answer.isCorrect)
            for (const a of temp)
                a.isCorrect = false
            temp[index] = answer
            return temp
        })
    }

    return (
        <>
            <TextField
                required
                label="Question"
                value={label}
                onChange={(e) => {
                    setLabel(e.target.value)
                }}
            />
            <Box sx={{
                display: "flex",
                alignItems: "center"
            }}>
                <Typography sx={{mr: "1rem"}}>Réponses</Typography>
                <Tooltip title="Ajouter">
                    <IconButton onClick={add}>
                        <Add color="primary"/>
                    </IconButton>
                </Tooltip>
            </Box>
            {answers.map((answer, index) => (
                <Box
                    key={`${label}.${index}`}
                    sx={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        mb: 1
                    }}>
                    <Radio
                        checked={answer.isCorrect}
                        onChange={e => handleAnswerChange(
                            {...answer, isCorrect: e.target.checked},
                            index
                        )}
                    />
                    <TextField
                        required
                        label="Réponse"
                        color={answer.isCorrect ? "success" : "error"}
                        focused
                        value={answer.label}
                        onChange={e => handleAnswerChange(
                            {...answer, label: e.target.value},
                            index
                        )}
                    />
                    <Tooltip title="Supprimer">
                        <div>
                            <IconButton disabled={answers.length == 1} onClick={() => remove(index)}>
                                <DeleteIcon color={answers.length == 1 ? "disabled" : "secondary"} />
                            </IconButton>
                        </div>
                    </Tooltip>
                    <Tooltip title="monter">
                        <div>
                            <IconButton disabled={index === 0} onClick={() => moveAnswerUp(index)}>
                                <ArrowUpward/>
                            </IconButton>
                        </div>
                    </Tooltip>
                    <Tooltip title="descendre">
                        <div>
                            <IconButton disabled={index === answers.length - 1}
                                        onClick={() => moveAnswerDown(index)}>
                                <ArrowDownward/>
                            </IconButton>
                        </div>
                    </Tooltip>
                </Box>
            ))}
        </>
    )
}
