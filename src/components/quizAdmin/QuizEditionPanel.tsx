import {generatePath, useParams, useNavigate, Link} from "react-router-dom";
import * as React from 'react';
import {
    alpha,
    Box,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Divider,
    FormControl,
    FormControlLabel,
    FormLabel,
    Grid,
    IconButton,
    InputBase,
    InputLabel,
    NativeSelect,
    Radio,
    RadioGroup,
    styled,
    Tab,
    Tabs,
    TextField,
    Tooltip,
    Typography
} from "@mui/material";
import {DataGrid, GridActionsCellItem, GridRowId} from "@mui/x-data-grid";
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import SearchIcon from '@mui/icons-material/Search';
import {Add, ArrowDownward, ArrowUpward} from "@mui/icons-material";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import useRequest from "../../modules/useRequest";
import QuizBox from "./QuizBox";
import {getDefaultQuiz} from "../../modules/utils";
import {QuizType} from "../../modules/types";

const Search = styled('div')(({theme}) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
        backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(1),
        width: '100%',
    },
}));

const SearchIconWrapper = styled('div')(({theme}) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({theme}) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '12ch',
            '&:focus': {
                width: '20ch',
            },
        },
    },
}));

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function TabPanel(props: TabPanelProps) {
    const {children, value, index, ...other} = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{p: 3}}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export default function QuizEditionPanel() {
    const {id} = useParams();
    const {get} = useRequest()

    const [quiz, setQuiz] = React.useState<QuizType | null>(null)

    async function updateQuiz() {
        const quiz: QuizType = await get(`/quizz/byId/${id}`);
        quiz.questions = await get(`/question/byQuizzId/${id}`)
        for (const question of quiz.questions) {
            question.answers = await get(`/answer/byQuestionId/${question.id}`)
        }
        console.log(quiz)
        setQuiz(quiz)
    }

    React.useEffect(() => {
        updateQuiz()
    }, [])


    const [rows, setRows] = React.useState([{
        id: 1,
        firstname: "",
        lastname: "",
        worstScore: 0,
        meanScore: 0,
        bestScore: 0,
        rank: ""
    }]);

    React.useEffect(() => {
        (async () => {
            try {

                //récupérer infos sur les parcours du user
                const data = await get(`/quizz/getRankingsList/${id}`)
                const detailsRows = []
                for (const d of data) {
                    const [rank1, rank2] = await get(`/quizz/ranking/${id}/${d.user.id}`)
                    detailsRows.push({
                        id: d.id,
                        firstname: d.user.firstname,
                        lastname: d.user.lastname,
                        worstScore: d.worstScore,
                        meanScore: d.meanScore,
                        bestScore: d.bestScore,
                        rank: rank1 + "/" + rank2
                    })
                }
                setRows(detailsRows)

            } catch (e) {
                console.error(e)
            }
        })()
    }, [])


    const [value, setValue] = React.useState(0);
    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
    };

    const [answerValue, setAnswerValue] = React.useState('3');

    const handleAnswerChange = (event: React.ChangeEvent<HTMLInputElement>, newAnswer: string) => {
        setAnswerValue(newAnswer);
        console.log(newAnswer);
    };

    const navigate = useNavigate();
    const goToUserPage = (id: GridRowId) => {
        console.log("Going to user " + id);
        navigate(`/admin_board_users/${id}`);
    };

    const deleteUser = React.useCallback(
        (id: GridRowId) => () => {
            console.log('Deleting ' + id)
        },
        [],
    );

    const editUser = React.useCallback(
        (id: GridRowId) => () => {
            goToUserPage(id);
        },
        [],
    );

    const columns = React.useMemo(
        () => [
            {field: 'id', headerName: 'ID', minWidth: 70, flex: 1},
            {field: 'firstname', headerName: 'Prénom', minWidth: 170, flex: 1},
            {field: 'lastname', headerName: 'Nom', minWidth: 100, flex: 1},
            {field: 'worstScore', headerName: 'Pire score', minWidth: 100, flex: 1},
            {field: 'meanScore', headerName: 'Score moyen', minWidth: 100, flex: 1},
            {field: 'bestScore', headerName: 'Meilleur score', minWidth: 100, flex: 1},
            {field: 'rank', headerName: 'Classement', minWidth: 100, flex: 1},
        ], [editUser, deleteUser]);

    /*const rows = [
        { id: 7, firstname: 'Harry', lastname: 'Potter', score: '80%', best_score: '86%', rank: '2/4' },
        { id: 9, firstname: 'Hermione', lastname: 'Granger', score: '92%', best_score: '92%', rank: '1/9' },
    ];*/


    let styleAnswer = function (answerValue: string, radioValue: string) {
        console.log("Answer value: " + answerValue);
        console.log("Radio value: " + radioValue);
        if (answerValue === radioValue)
            return "success";
        else
            return "secondary";
    };

    const [open, setOpen] = React.useState(false);

    const handleOpenTheme = () => {
        setOpen(true);
    };

    const handleCloseTheme = () => {
        setOpen(false);
    };


    return (
        <div style={{
            // display: "flex",
            flexDirection: "column",
            alignItems: "center",
            paddingLeft: "10%",
            paddingRight: "10%"
        }}>
            <Tooltip title="Retour à la gestion des quizz">
                <IconButton component={Link} to="/admin_board_quizz">
                    <ArrowBackIcon color="primary"/>
                </IconButton>
            </Tooltip>
            <Typography variant="h4" gutterBottom component="div">
                {`Quizz (id: ${id})`}
            </Typography>
            <Box sx={{width: '100%'}}>
                <Box sx={{borderBottom: 1, borderColor: 'divider'}}>
                    <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                        <Tab label="Contenu du quizz" {...a11yProps(0)} />
                        <Tab label="Enregistrements" {...a11yProps(1)} />
                    </Tabs>
                </Box>
                <TabPanel value={value} index={0}>
                    <Box sx={{
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center"
                    }}>
                        {quiz &&
                            <QuizBox
                                mode="update"
                                defaultQuiz={quiz}
                            />
                        }
                    </Box>
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <Search>
                        <SearchIconWrapper>
                            <SearchIcon/>
                        </SearchIconWrapper>
                        <StyledInputBase
                            placeholder="Rechercher…"
                            inputProps={{'aria-label': 'search'}}
                        />
                    </Search>
                    <div style={{height: 300, width: '100%'}}>
                        <DataGrid
                            rows={rows}
                            columns={columns}
                            pageSize={10}
                            rowsPerPageOptions={[5]}
                            disableColumnMenu
                            onRowClick={(rowData) => goToUserPage(rowData.id)}
                        />
                    </div>
                </TabPanel>
            </Box>
        </div>
    );
}