import {Box, Button, Divider, TextField} from "@mui/material";
import ThemeSelector from "./theme/ThemeSelector";
import QuestionSelector from "./QuestionSelector";
import QuestionEditor from "./QuestionEditor";
import * as React from "react";
import useAuthentication from "../../modules/useAuthentication";
import useRequest from "../../modules/useRequest";
import {useNavigate} from "react-router-dom";
import {QuestionType, QuizType, Theme} from "../../modules/types";
import {ensure, getDefaultQuestion} from "../../modules/utils";

export interface QuizBoxProps {
    mode: "add" | "update",
    defaultQuiz: QuizType
}

export default function QuizBox(props: QuizBoxProps) {
    const {user} = useAuthentication()
    const {post, put, del} = useRequest()
    const navigate = useNavigate()


    const [title, setTitle] = React.useState(props.defaultQuiz.title);
    const [selectedTheme, setSelectedTheme] = React.useState<Theme | null>(props.defaultQuiz.theme ?? null)
    const [questions, setQuestions] = React.useState<QuestionType[]>(props.defaultQuiz.questions)
    const [selectedQuestionIndex, setSelectedQuestionIndex] = React.useState<number>(0)
    const [selectedQuestion, setSelectedQuestion] = React.useState<QuestionType>(questions[0])

    const [removeQuestions, setRemoveQuestions] = React.useState<number[]>([])
    const [removeAnswers, setRemoveAnswers] = React.useState<number[]>([])

    console.log("selectedtheme", selectedTheme)
    React.useEffect(() => {
        setSelectedQuestion(questions[selectedQuestionIndex])
    }, [selectedQuestionIndex])

    function setupPositions(quiz: QuizType) {
        quiz.questions.forEach((question, index) => {
            question.position = index + 1
            question.answers.forEach((answer, index) => {
                answer.position = index + 1
            })
        })
    }

    async function updateQuizRequest(quiz: QuizType) {
        console.log("updating quizz : ", quiz)
        try {
            //removing before edit
            for (const id of removeAnswers) {
                await del(`/answer/byId/${id}`)
            }
            for (const id of removeQuestions) {
                await del(`/question/byId/${id}`)
            }
            const {id: quizzId} = await put(`/quizz/update`, {
                ...quiz,
                creatorId: user?.id,
                themeId: quiz.theme?.id
            });
            for (const question of quiz.questions) {
                let questionId
                if (question.id) {
                    questionId = (await put(`/question/update`, {...question, quizzId})).id
                } else {
                    questionId = (await post(`/question/add`, {...question, quizzId})).id
                }
                for (const answer of question.answers) {
                    if (answer.id) {
                        await put(`/answer/update`, {...answer, questionId})
                    } else {
                        await post(`/answer/add`, {...answer, questionId})
                    }
                }
            }
            setRemoveAnswers([])
            setRemoveQuestions([])
        } catch (response: any) {
            if (response.status === 406) {
            }
            console.log("trigger duplication question");
            console.log("response", response);
        }
    }

    async function createQuizRequest(quiz: QuizType) {
        const {id: quizzId} = await post(`/quizz/add`, {
            ...quiz,
            creatorId: user?.id,
            themeId: quiz.theme?.id
        })
        for (const question of quiz.questions) {
            const {id: questionId} = await post(`/question/add`, {...question, quizzId})
            for (const answer of question.answers) {
                await post(`/answer/add`, {...answer, questionId})
            }
        }
    }

    async function handleQuiz() {
        if (!selectedTheme) throw new Error("theme is null")
        const quiz: QuizType = {
            id: props.defaultQuiz.id,
            title,
            questions,
            isActive: true,
            theme: ensure(selectedTheme)
        }
        setupPositions(quiz)
        switch (props.mode) {
            case "add":
                await createQuizRequest(quiz)
                break
            case "update":
                await updateQuizRequest(quiz)
                break
            default:
                throw new Error("this error should never be thrown")
        }
        navigate("/admin_board_quizz")
    }

    function handleNewQuestion() {
        const newIndex = questions.length
        setQuestions(old => {
            return [...old, getDefaultQuestion()]
        })
        setSelectedQuestionIndex(newIndex)
    }

    function handleQuestionChange(question: QuestionType) {
        setQuestions(old => {
            const temp = [...old]
            temp[selectedQuestionIndex] = question
            return temp
        })
    }


    return (
        <Box sx={{
            width: "55%",
            minWidth: "25rem",
            display: "flex",
            flexDirection: "column",
            alignItems: "stretch",
            border: '1px solid grey',
            padding: 1,
            borderRadius: "5px",
            '& > *, & > .MuiTextField-root': {mb: 1},
        }}>
            <TextField
                required
                label="Titre"
                value={title}
                onChange={(e) => {
                    setTitle(e.target.value);
                }}
            />
            <ThemeSelector
                defaultTheme={props.defaultQuiz.theme}
                onThemeChanged={theme => {
                    console.log("theme changed to ", theme)
                    setSelectedTheme(theme)
                }}/>
            <Divider/>
            <QuestionSelector
                index={selectedQuestionIndex}
                questions={questions}
                onIndexChange={setSelectedQuestionIndex}
                onRemove={(id?: number) => {
                    if (!id) return
                    setRemoveQuestions([...removeQuestions, id])
                }}
                setQuestions={setQuestions}
            />
            <QuestionEditor
                question={selectedQuestion}
                onQuestionChange={handleQuestionChange}
                onRemove={(id?: number) => {
                    if (!id) return
                    setRemoveAnswers([...removeAnswers, id])
                }}
            />
            <Button variant="contained" size="large" color="inherit"
                    style={{
                        width: "100%",
                        marginBottom: 10
                    }}
                    onClick={handleNewQuestion}
            >Nouvelle question</Button>
            <Button
                variant="contained"
                size="large"
                style={{
                    width: "100%",
                }}
                onClick={handleQuiz}>
                {props.mode === "add" ? "Créer le quizz" : "mette à jour le quizz"}
            </Button>
        </Box>
    )
}