import {
    Box,
    IconButton, MenuItem, Select,
    Tooltip,
} from "@mui/material";
import {ArrowDownward, ArrowUpward} from "@mui/icons-material";
import DeleteIcon from "@mui/icons-material/Delete";
import * as React from "react";
import {QuestionType} from "../../modules/types";

export interface QuestionSelectorProps {
    index: number
    questions: QuestionType[]
    onIndexChange: (index: number) => void
    onRemove: (id?: number) => void
    setQuestions: React.Dispatch<React.SetStateAction<QuestionType[]>>
}

export default function QuestionSelector(props: QuestionSelectorProps) {

    function moveQuestionDown() {
        if (props.index < 0 || props.index >= props.questions.length - 1) return
        props.setQuestions(old => {
            const temp = [...old]
            temp.splice(props.index + 1, 0, temp.splice(props.index, 1)[0])
            return temp
        })
        props.onIndexChange(props.index + 1)
    }

    function moveQuestionUp() {
        if (props.index < 1 || props.index >= props.questions.length) return
        props.setQuestions(old => {
            const temp = [...old]
            temp.splice(props.index - 1, 0, temp.splice(props.index, 1)[0])
            return temp
        })
        props.onIndexChange(props.index - 1)
    }

    function remove() {
        if (props.index < 0 || props.index >= props.questions.length) return
        if (props.questions.length <= 1) return
        props.onRemove(props.questions[props.index].id)
        props.setQuestions(old => {
            const temp = [...old]
            temp.splice(props.index, 1)
            return temp
        })
        props.onIndexChange(props.index !== 0 ? props.index - 1 : 0)
    }

    return (
        <Box sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center"
        }}>

            <Select
                sx={{
                    width: "10rem"
                }}
                value={props.index}
                onChange={(e) => {
                    props.onIndexChange(e.target.value as number)
                }}
            >
                {props.questions.map((question, index) => (
                    <MenuItem
                        key={index}
                        value={index}
                    >
                        {index + 1}: {question.label}
                    </MenuItem>
                ))}
            </Select>
            <Tooltip title="Monter">
                <div>
                    <IconButton onClick={moveQuestionUp} disabled={props.index === 0}>
                        <ArrowUpward />
                    </IconButton>
                </div>
            </Tooltip>
            <Tooltip title="Descendre">
                <div>
                    <IconButton
                        onClick={moveQuestionDown}
                        disabled={props.index === props.questions.length - 1}
                    >
                        <ArrowDownward />
                    </IconButton>
                </div>
            </Tooltip>
            <Tooltip title="Supprimer">
                <div>
                    <IconButton
                        onClick={remove}
                        disabled={props.questions.length === 1}
                    >
                        <DeleteIcon color={props.questions.length === 1 ? "disabled" : "secondary"} />
                    </IconButton>
                </div>
            </Tooltip>
        </Box>
    )
}