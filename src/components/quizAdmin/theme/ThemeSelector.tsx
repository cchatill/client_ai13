import {Box, Button, FormControl, InputLabel, MenuItem, Select} from "@mui/material";
import DialogTheme from "./DialogTheme";
import * as React from "react";
import {Theme} from "../../../modules/types";
import useRequest from "../../../modules/useRequest";
import {undefAsNull} from "../../../modules/utils";

export interface ThemeSelectorProps {
    defaultTheme?: Theme
    onThemeChanged: (theme: Theme | null) => void
}

export default function ThemeSelector(props: ThemeSelectorProps) {
    const {get} = useRequest()
    const [open, setOpen] = React.useState(false)
    const [themes, setThemes] = React.useState<Theme[]>([])
    const [selectedThemeId, setSelectedThemeId] = React.useState<number | null>(props.defaultTheme?.id ?? null)

    async function updateList() {
        setThemes(await get("/theme/all"))
    }

    // update theme list on component loading
    React.useEffect(() => {
        updateList()
    }, [])

    // notify changed selected theme to the parent component
    React.useEffect(() => {
        if (!themes) return
        props.onThemeChanged(undefAsNull(themes.find(t => t.id === selectedThemeId)))
    }, [selectedThemeId, themes])

    return (
        <Box sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            flex: "1"
        }}>
            <FormControl
                variant="standard"
                style={{minWidth: "30%"}}
            >
                <InputLabel>
                    Thème
                </InputLabel>
                <Select
                    value={selectedThemeId ?? ""}
                    onChange={(e) => {
                        setSelectedThemeId(e.target.value as number)
                    }}
                    inputProps={{
                        name: 'age',
                        id: 'uncontrolled-native',
                    }}
                >
                    {themes.map(theme => (
                        <MenuItem key={theme?.id} value={theme?.id}>{theme.label}</MenuItem>
                    ))}
                </Select>
            </FormControl>
            <Button variant="contained" size="large" onClick={() => setOpen(true)}>
                Nouveau thème
            </Button>
            <DialogTheme
                open={open}
                handleClose={() => setOpen(false)}
                onThemeChanged={(newTheme) => {
                    updateList()
                    setSelectedThemeId(newTheme?.id ? newTheme.id : 0)
                }}
            />
        </Box>


    )
}