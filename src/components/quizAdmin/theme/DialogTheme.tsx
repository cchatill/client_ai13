import {Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField} from "@mui/material";
import * as React from "react";
import {Theme} from "../../../modules/types";
import useRequest from "../../../modules/useRequest";

export interface DialogThemeProps {
    open: boolean
    handleClose: () => void
    onThemeChanged: (theme: Theme) => void
}

export default function DialogTheme(props: DialogThemeProps) {
    const {post} = useRequest()

    const [label, setLabel] = React.useState("")

    async function handleCreateTheme() {
        const newTheme = await post("/theme/add", {
            label
        })
        props.onThemeChanged(newTheme)
        props.handleClose()
    }

    React.useEffect(() => {
        setLabel("")
    }, [])

    return (
        <Dialog open={props.open}>
            <DialogTitle>Création d'un nouveau thème</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Veuillez saisir le nom du nouveau thème.
                </DialogContentText>
                <TextField
                    value={label}
                    onChange={(e) => setLabel(e.target.value)}
                    autoFocus
                    margin="dense"
                    label="Thème"
                    type="string"
                    fullWidth
                    variant="standard"
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={() => props.handleClose()}>Annuler</Button>
                <Button onClick={handleCreateTheme}>Créer le thème</Button>
            </DialogActions>
        </Dialog>
    )
}