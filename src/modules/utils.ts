import {AnswerType, QuestionType, QuizType} from "./types";

export function undefAsNull<T>(el: T | undefined): T | null {
    return el === undefined ? null : el
}

export function ensure<T>(el?: T): T {
    if (el == null)
        throw new Error("value is suposed to be here")
    return el
}

export function getDefaultQuiz(): QuizType {
    return {
        title: "",
        isActive: true,
        questions: [getDefaultQuestion()]
    }
}

export function getDefaultQuestion(): QuestionType {
    return {
        label: "",
        isActive: true,
        position: 0,
        answers: [{
            label: "",
            isCorrect: false,
            isActive: true,
            position: 0
        }]
    }
}

export function getDefaultAnswer(): AnswerType {
    return {
        label: "",
        isCorrect: false,
        isActive: true,
        position: 0

    }
}