export type User = {
    id: number,
    firstname: string,
    lastname: string,
    email: string,
    role: "TRAINEE" | "ADMIN"
}

export type ReturnType = {
    connected: boolean
    token: string | null,
    user: User | null
    connect: (email: string, password: string) => void
    disconnect: () => void
}

export type ResponseType = {
    id? : number,
    label : string,
    isCorrect? : boolean,
    position : number,
    isSelected? : string
}

export type Theme = {
    id?: number;
    label: string
}

export type QuizType = {
    id?: number,
    theme?: Theme,
    title: string,
    isActive: boolean,
    questions: QuestionType[]
}

export type QuestionType = {
    id?: number,
    label: string,
    isActive: boolean,
    position: number,
    answers: AnswerType[]
}

export type AnswerType = {
    id?: number,
    label: string,
    isCorrect: boolean,
    isActive: boolean,
    position: number,
}

export type correctType = "success" | "error" | "inherit" | "disabled" | "action" | "primary" | "secondary" | "info" | "warning" | undefined