import AppBar from "../components/AppBar";
import CreateUserPanel from "../components/CreateUserPanel";
import Admin from "../components/redirecters/Admin";

export default function CreateUser() {
    return (
        <Admin> <AppBar /> <CreateUserPanel /></Admin>);
}