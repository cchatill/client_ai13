import AppBar from "../components/AppBar";
import QuizCreationPanel from "../components/quizAdmin/QuizCreationPanel";
import Admin from "../components/redirecters/Admin";

export default function CreateQuizz() {
    return (
        <Admin> <AppBar /> <QuizCreationPanel /></Admin>);
}