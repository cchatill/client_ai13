import React from "react";
import AppBar from "../components/AppBar";
import QuizzPanel from "../components/QuizzPanel";
import Trainee from "../components/redirecters/Trainee";

export default function Quizz() {
    return (
        <Trainee> <AppBar /> <QuizzPanel /></Trainee>);
}