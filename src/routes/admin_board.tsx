import React from "react";
import AdminMainPanel from "../components/AdminMainPanel";
import AppBar from "../components/AppBar";
import Admin from "../components/redirecters/Admin";

export default function AdminBoard() {
    return (
        <Admin> <AppBar /> <AdminMainPanel /></Admin>);
}