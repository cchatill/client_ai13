import React from "react";
import AppBar from "../components/AppBar";
import TraineeResultsPanel from "../components/TraineeResults";
import Trainee from "../components/redirecters/Trainee";

export default function TraineeResults() {
    return (
        <Trainee> <AppBar /> <TraineeResultsPanel /></Trainee>);
}