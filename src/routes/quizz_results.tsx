import React from "react";
import AppBar from "../components/AppBar";
import { QuizzResultsPanel } from "../components/QuizzResultsPanel";
import Trainee from "../components/redirecters/Trainee";

export default function QuizzResults() {
    return (
        <Trainee> <AppBar /> <QuizzResultsPanel /></Trainee>);
}