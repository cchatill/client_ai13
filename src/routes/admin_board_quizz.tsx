import React from "react";
import AdminQuizzPanel from "../components/AdminQuizzPanel"
import AppBar from "../components/AppBar";
import Admin from "../components/redirecters/Admin";

export default function AdminBoardQuizz() {
    return (
        <Admin> <AppBar /> <AdminQuizzPanel /></Admin>);
}