import React from "react";
import AdminUsersPanel from "../components/AdminUsersPanel"
import AppBar from "../components/AppBar";
import Admin from "../components/redirecters/Admin";

export default function AdminBoardUsers() {
    return (
        <Admin> <AppBar /> <AdminUsersPanel /></Admin>);
}