import React from "react";
import AdminEditUserPanel from "../components/AdminEditUserPanel";
import AppBar from "../components/AppBar";
import Admin from "../components/redirecters/Admin";

export default function AdminBoardEditUser() {
    return (
        <Admin> <AppBar /> <AdminEditUserPanel /></Admin>);
}