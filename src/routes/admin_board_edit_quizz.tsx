import React from "react";
import AppBar from "../components/AppBar";
import Admin from "../components/redirecters/Admin";
import QuizCreationPanel from "../components/quizAdmin/QuizCreationPanel";
import {getDefaultQuiz} from "../modules/utils";
import QuizEditionPanel from "../components/quizAdmin/QuizEditionPanel";

export default function AdminBoardEditQuizz() {
    return (
        <Admin> <AppBar /> <QuizEditionPanel /></Admin>);
}