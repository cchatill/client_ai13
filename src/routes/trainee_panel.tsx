import React from "react";
import TraineeMainPanel from "../components/TraineeMainPanel";
import AppBar from "../components/AppBar";
import Trainee from "../components/redirecters/Trainee";

export default function TraineePanel() {
    return (
        <Trainee> <AppBar /> <TraineeMainPanel /></Trainee>);
}