import React from "react";
import AppBar from "../components/AppBar";
import LoginForm from "../components/LoginForm";

export default function LoginPage() {
    return (<div><AppBar /><LoginForm /></div>);
}