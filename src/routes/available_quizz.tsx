import React from "react";
import AppBar from "../components/AppBar";
import AvailableQuizzPanel from "../components/AvailableQuizzPanel";
import Trainee from "../components/redirecters/Trainee";

export default function AvailableQuizz() {
    return (
        <Trainee> <AppBar /> <AvailableQuizzPanel /></Trainee>);
}