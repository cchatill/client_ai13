import React from "react";
import { createTheme, ThemeProvider as MUIThemeProvider } from "@mui/material/styles";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Root from "./routes/root";
import AdminBoard from "./routes/admin_board";
import LoginPage from "./routes/login";
import AdminBoardUsers from "./routes/admin_board_users";
import AdminBoardQuizz from "./routes/admin_board_quizz";
import AdminBoardEditUser from "./routes/admin_board_edit_user";
import AdminBoardEditQuizz from "./routes/admin_board_edit_quizz";
import CreateQuizz from "./routes/create_quizz";
import CreateUser from "./routes/create_user";
import TraineePanel from "./routes/trainee_panel";
import TraineeResults from "./routes/trainee_results";
import QuizzResults from "./routes/quizz_results";
import AvailableQuizz from "./routes/available_quizz";
import Quizz from "./routes/quizz";
import Unauthorized from "./routes/unauthorized";

const darkTheme = createTheme({
    palette: {
        primary: {
            main: 'rgba(0, 0, 0, 0.87)'
        },
        secondary: {
            main: '#ba000d'
        }
    },
});

export default function App() {


    return (
        <MUIThemeProvider theme={darkTheme}>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Root />} />
                    <Route path="/login" element={<LoginPage />} />
                    <Route path="/admin_board" element={<AdminBoard />} />
                    <Route path="/admin_board_users" element={<AdminBoardUsers />} />
                    <Route path="/admin_board_quizz" element={<AdminBoardQuizz />} />
                    <Route path="/admin_board_users/:id" element={<AdminBoardEditUser />}/>
                    <Route path="/admin_board_quizz/:id" element={<AdminBoardEditQuizz />}/>
                    <Route path="/admin_board/create_quizz" element={<CreateQuizz />} />
                    <Route path="/admin_board/create_user" element={<CreateUser />} />
                    <Route path="/trainee_panel" element={<TraineePanel />}></Route>
                    <Route path="/trainee_panel/results" element={<TraineeResults />}></Route>
                    <Route path="/trainee_panel/available_quizz" element={<AvailableQuizz />}></Route>
                    <Route path="/trainee_panel/quizz_results/:id" element={<QuizzResults />}/>
                    <Route path="/trainee_panel/quizz/:id" element={<Quizz />}/>
                    <Route path="/unauthorized" element={<Unauthorized />}/>
                </Routes>
            </BrowserRouter>
        </MUIThemeProvider>

    );
}
